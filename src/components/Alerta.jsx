import React from 'react';
import PropTypes from 'prop-types';

const Alerta = ({ mensaje }) => {
  return (
    <div className="alert alert-danger mt-3 text-center" role="alert">
      <b>Ups!!!</b> { mensaje }.
    </div>
  );
};

Alerta.propTypes = {
  mensaje: PropTypes.string.isRequired
};

export default Alerta;