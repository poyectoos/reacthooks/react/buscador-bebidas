import React, { Fragment, useContext } from 'react';

import Receta from './Receta';

import { RecetasContext } from '../contexts/Recetas';

const Recetas = () => {

  const { recetas } = useContext(RecetasContext);

  if (recetas.length === 0) return null;

  return (
    <Fragment>
      <h2 className="text-center">Recetas</h2>
      <div className="row">
        {
          recetas.map(receta => (
            <Receta
              key={ receta.idDrink }
              receta={ receta }
            />
          ))
        }
      </div>
    </Fragment>
  );
};

export default Recetas;