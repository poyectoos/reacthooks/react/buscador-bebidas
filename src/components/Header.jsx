import React from 'react';

const Header = () => {
  return (
    <header className="bg-alert py-4 text-center">
      <h1 className="text-uppercase">Busca una bebida</h1>
      <h5 className="text-uppercase">Porque el kosako no es todo en la vida</h5>
    </header>
  );
};

export default Header;