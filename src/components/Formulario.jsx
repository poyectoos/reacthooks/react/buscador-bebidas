import React, { useState, useContext } from 'react';

import Alerta from './Alerta';

import { CategoriasContext } from '../contexts/Categorias';
import { RecetasContext } from '../contexts/Recetas';

const Formulario = () => {

  const [ parametros, actualizarParametros ] = useState({
    ingrediente: '',
    categoria: '',
  });
  const [ error, actualizarError ] = useState(false);
  
  const { ingrediente, categoria } = parametros;

  const { categorias } = useContext(CategoriasContext);
  const { actualizarBusqueda } = useContext(RecetasContext);

  const handleSubmit = e => {
    e.preventDefault();
    if (ingrediente.trim() === '' || categoria.trim() === '') {
      actualizarError(true);
      return;
    }
    actualizarError(false);

    actualizarBusqueda(parametros);
  }

  const handleChange = e => {
    actualizarParametros({
      ...parametros,
      [ e.target.name ] : e.target.value
    })
  }

  return (
    <form
      onSubmit={ handleSubmit }
      className="col-md-12"
    >
      <fieldset className="text-center">
        <legend>
          ¿Qué ingrediente estas buscando?
        </legend>
      </fieldset>

      <div className="row mt-3">
        <div className="col-md-4 mb-2">
          <input
            type="text"
            name="ingrediente"
            className="form-control"
            placeholder="Whisky o???"
            value= { ingrediente }
            onChange={ handleChange }
          />
        </div>
        <div className="col-md-4 mb-2">
          <select
            name="categoria"
            className="form-control"
            value= { categoria }
            onChange={ handleChange }
          >
            <option value="" disabled>Selecciona una categoria</option>
            {
              categorias.map(categoria => (
                <option
                  key={ categoria.strCategory }
                  value={ categoria.strCategory }
                >
                  { categoria.strCategory }
                </option>
              ))
            }
          </select>
        </div>
        <div className="col-md-4 mb-2">
          <button
            className="btn btn-primary btn-block"
          >
            Buscar
          </button>
        </div>
        {
          error
            ?
          <div className="col-12">
            <Alerta mensaje="Proporciona un ingrediente y una categoria" />
          </div>
            :
          null
        }
      </div>
    </form>
  );
};

export default Formulario;