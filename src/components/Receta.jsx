import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import { makeStyles } from '@material-ui/core/styles';

import { DetallesContext } from '../contexts/Detalles';

// Estilos para el modal
function getModalStyle() {
  const top = 50 ;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}
const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: `80%`,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  scroll: {
    maxHeight: '500px',
    overflowY: 'scroll',
    overflowX: 'none'
  }
}));

const Receta = ({ receta }) => {

  const { detalle, actualizarId } = useContext(DetallesContext);
  // Estados para el uso del modal
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);
  // Creamos una instancia de las clases
  const classes = useStyles();

  const { idDrink, strDrink, strDrinkThumb } = receta;
  const { strAlcoholic, strInstructions, strInstructionsES } = detalle;

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    actualizarId(null);
    setOpen(false);
  };
  const handleChangeId = () => {
    actualizarId(idDrink);
    handleOpen();
  }

  const listaIngredientes = (ingredientes) => {
    let listado = [];
    for (let i = 1; i <= 15; i++) {
      if (ingredientes[`strIngredient${i}`]) {
        listado.push(
          <li>
            <b>{ ingredientes[`strIngredient${i}`] }: </b>
            { ingredientes[`strMeasure${i}`] ? ingredientes[`strMeasure${i}`] : null }
          </li>
        );
      }
    }
    return listado;
  }

  return (
    <div className="col-12 col-sm-6 col-md-4 col-lg-3 p-1">
      <div className="card">
        <img
          src={ strDrinkThumb }
          className="card-img-top"
          alt={ strDrink }
        />
        <div className="card-body p-3">
          <h5 className="card-title my-0 py-0">
            {
              strDrink
                ?
              <span className="truncate">{ strDrink }</span>
                :
              'La que no puede ser nombrada'
            }
          </h5>
        </div>
        <div className="card-footer p-3">
          <button
            className="btn btn-primary btn-block"
            onClick={ handleChangeId }
          >
            Ver receta
          </button>

          <Modal
            open={ open }
            onClose={ handleClose }
          >
            <div
              style={ modalStyle }
              className={classes.paper}
            >
              <h3 className="text-center text-dark border-bottom pb-2 my-0">{ strDrink }</h3>
              <div className={ `row text-dark mt-2 ${classes.scroll}` }>
                <div className="col-12 col-md-4 col-lg-3 text-center">
                  <h5>
                    <b>Tipo: </b>
                    <span
                      className={ strAlcoholic === 'Alcoholic' ? 'text-primary' : 'text-success' }
                    >
                      { strAlcoholic }
                    </span>
                  </h5>
                  <img
                    src={ strDrinkThumb }
                    alt={ strDrink }
                    className="img-fluid mb-3"
                  />
                </div>
                <div className="col-12 col-md-8 col-lg-9">
                  <div className="row">
                    <div className="col-12 mb-1">
                      <h5>Ingredientes</h5>
                      <ul>
                        { listaIngredientes(detalle) }
                      </ul>
                    </div>
                    <div className="col-12">
                      <h5>Instrucciones</h5>
                      <p>
                        { 
                          strInstructionsES
                            ?
                          strInstructionsES
                            :
                          strInstructions
                        }
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  );
};

Receta.propTypes = {
  receta: PropTypes.object.isRequired
};

export default Receta;