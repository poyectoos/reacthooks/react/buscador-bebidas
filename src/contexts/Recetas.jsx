import axios from 'axios';
import { createContext, useState, useEffect } from 'react';

export const RecetasContext = createContext();

const RecetasProvider = (props) => {
  
  const [ busqueda, actualizarBusqueda ] = useState({
    ingrediente: '',
    categoria: '',
  });
  const [ recetas, actualizarRecetas ] = useState([]);


  const { ingrediente, categoria } = busqueda;

  useEffect(() => {
    if (ingrediente === '' || categoria === '') return;
    const consultar = async () => {
      const URI = `https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=${ingrediente}&c=${categoria}`;
      
      const data = await axios.get(URI);

      if (data.status === 200) {
        actualizarRecetas(data.data.drinks)
      } else {
        console.error('No status code for Dinks');
      }
    }
    consultar();
    // eslint-disable-next-line
  }, [busqueda]);


  return (
    <RecetasContext.Provider
      value={{
        recetas,
        actualizarBusqueda
      }}
    >
      { props.children }
    </RecetasContext.Provider>
  );
}

export default RecetasProvider;