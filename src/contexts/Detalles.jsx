import { createContext, useEffect, useState } from 'react';
import axios from 'axios';

export const DetallesContext = createContext();

const DetallesProvider = (props) => {

  const [ id, actualizarId ] = useState(null);
  const [ detalle, actualizarDetalle ] = useState({});

  useEffect(() => {
    if (!id) return;
    consultar();
    // eslint-disable-next-line
  }, [id]);

  const consultar = async () => {
    const URI = `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`;

    actualizarDetalle({});

    const data = await axios.get(URI);

    if (data.status === 200) {
      actualizarDetalle(data.data.drinks[0]);
    } else {
      console.error('No status code for Detalles');
    }
  }

  return (
    <DetallesContext.Provider
      value={{
        detalle,
        actualizarId
      }}
    >
      { props.children }
    </DetallesContext.Provider>
  );
}

export default DetallesProvider;