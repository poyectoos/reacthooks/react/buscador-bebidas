import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';

// Se crea el context

export const CategoriasContext = createContext();

const CategoriasProvider = (props) => {

  const [ categorias, actualizarCategorias ] = useState([]);
  
  useEffect(() => {
    const consultar = async () => {
      const API = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list';

      const data = await axios.get(API);
      
      if (data.status === 200) {
        actualizarCategorias(data.data.drinks);
      } else {
        console.error('No status code for Categories');
      }
    }
    consultar();
  }, []);

  return(
    <CategoriasContext.Provider
      value={{
        categorias
      }}
    >
      { props.children }
    </CategoriasContext.Provider>
  );


}

export default CategoriasProvider;