
import Header from './components/Header';
import Formulario from './components/Formulario';
import Recetas from './components/Recetas';

import CategoriasProvider from './contexts/Categorias';
import RecetasProvider from './contexts/Recetas';
import DetallesProvider from './contexts/Detalles';

function App() {
  return (
    <RecetasProvider>
      <CategoriasProvider>
        <DetallesProvider>
          <Header />

          <div className="container mt-3">
            <div className="row">
              <Formulario />
            </div>
          </div>

          <div className="container">
            <div className="mt-3 pb-5">
              <Recetas />
            </div>
          </div>
        </DetallesProvider>
      </CategoriasProvider>
    </RecetasProvider>
  );
}

export default App;
